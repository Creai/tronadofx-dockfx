/**
 * @file DockEvent.java
 * @brief Class implementing a generic base for docking events.
 *
 * @section License
 *
 * This file is a part of the DockFX Library. Copyright (C) 2015 Robert B. Colton
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program. If not, see <http:></http:>//www.gnu.org/licenses/>.
 */

package component.dock

import com.sun.javafx.scene.input.InputEventUtils

import javafx.event.Event
import javafx.event.EventTarget
import javafx.event.EventType
import javafx.geometry.Point3D
import javafx.scene.Node
import javafx.scene.input.PickResult

/**
 * Base class for DockFX events. Each DockFX event has an associated event source, event target and
 * an event type. The event source specifies for an event handler the object on which that handler
 * has been registered and which sent the event to it. The event target defines the path through
 * which the event will travel when posted. The event type provides additional classification to
 * events of the same `DockEvent` class. Like a [MouseEvent] the event will be
 * associated with an x and y coordinate local to the node as well as to the screen.
 *
 * @since DockFX 0.1
 */
class DockEvent
/**
 * Constructs new DockEvent event..
 *
 * @param source the source of the event. Can be null.
 * @param target the target of the event. Can be null.
 * @param eventType The type of the event.
 * @param x The x with respect to the source. Should be in scene coordinates if source == null or
 * source is not a Node.
 * @param y The y with respect to the source. Should be in scene coordinates if source == null or
 * source is not a Node.
 * @param screenX The x coordinate relative to screen.
 * @param screenY The y coordinate relative to screen.
 * @param pickResult pick result. Can be null, in this case a 2D pick result without any further
 * values is constructed based on the scene coordinates
 * @param contents The contents being dragged during this event.
 */
@JvmOverloads constructor(
    source: Any?, target: EventTarget?, eventType: EventType<out DockEvent>,
    /**
     * Horizontal x position of the event relative to the origin of the `Scene` that contains
     * the DockEvent's node. If the node is not in a `Scene`, then the value is relative to the
     * boundsInParent of the root-most parent of the DockEvent's node.
     */
    /**
     * Returns horizontal position of the event relative to the origin of the `Scene` that
     * contains the DockEvent's source. If the node is not in a `Scene`, then the value is
     * relative to the boundsInParent of the root-most parent of the DockEvent's node. Note that in 3D
     * scene, this represents the flat coordinates after applying the projection transformations.
     *
     * @return horizontal position of the event relative to the origin of the `Scene` that
     * contains the DockEvent's source
     */
    val sceneX: Double,
    /**
     * Vertical y position of the event relative to the origin of the `Scene` that contains the
     * DockEvent's node. If the node is not in a `Scene`, then the value is relative to the
     * boundsInParent of the root-most parent of the DockEvent's node.
     */
    /**
     * Returns vertical position of the event relative to the origin of the `Scene` that
     * contains the DockEvent's source. If the node is not in a `Scene`, then the value is
     * relative to the boundsInParent of the root-most parent of the DockEvent's node. Note that in 3D
     * scene, this represents the flat coordinates after applying the projection transformations.
     *
     * @return vertical position of the event relative to the origin of the `Scene` that
     * contains the DockEvent's source
     */
    val sceneY: Double,
    /**
     * Absolute horizontal x position of the event.
     */
    /**
     * Returns absolute horizontal position of the event.
     *
     * @return absolute horizontal position of the event
     */
    val screenX: Double,
    /**
     * Absolute vertical y position of the event.
     */
    /**
     * Returns absolute vertical position of the event.
     *
     * @return absolute vertical position of the event
     */
    val screenY: Double, pickResult: PickResult?,
    /**
     * Information about the pick if the picked `Node` is a `Shape3D` node and its
     * pickOnBounds is false.
     */
    /**
     * Returns the contents of the dock event, similar to the dragboard.
     *
     * @return Node that is currently being dragged during this event
     */
    val contents: Node? = null
) : Event(source, target, eventType) {

    /**
     * Horizontal x position of the event relative to the origin of the DockEvent's node.
     */
    /**
     * Horizontal position of the event relative to the origin of the DockEvent's source.
     *
     * @return horizontal position of the event relative to the origin of the DockEvent's source.
     */
    @Transient
    var x: Double = 0.toDouble()
        private set

    /**
     * Vertical y position of the event relative to the origin of the DockEvent's node.
     */
    /**
     * Vertical position of the event relative to the origin of the DockEvent's source.
     *
     * @return vertical position of the event relative to the origin of the DockEvent's source.
     */
    @Transient
    var y: Double = 0.toDouble()
        private set

    /**
     * Depth z position of the event relative to the origin of the DockEvent's node.
     */
    /**
     * Depth position of the event relative to the origin of the DockEvent's source.
     *
     * @return depth position of the event relative to the origin of the DockEvent's source.
     */
    @Transient
    val z: Double

    /**
     * Information about the pick if the picked `Node` is a `Shape3D` node and its
     * pickOnBounds is false.
     */
    /**
     * Returns information about the pick.
     *
     * @return new PickResult object that contains information about the pick
     */
    val pickResult: PickResult

    /**
     * Constructs new DockEvent event..
     *
     * @param eventType The type of the event.
     * @param x The x with respect to the source. Should be in scene coordinates if source == null or
     * source is not a Node.
     * @param y The y with respect to the source. Should be in scene coordinates if source == null or
     * source is not a Node.
     * @param screenX The x coordinate relative to screen.
     * @param screenY The y coordinate relative to screen.
     * @param pickResult pick result. Can be null, in this case a 2D pick result without any further
     * values is constructed based on the scene coordinates
     */
    constructor(
        eventType: EventType<out DockEvent>, x: Double, y: Double, screenX: Double,
        screenY: Double, pickResult: PickResult
    ) : this(null, null, eventType, x, y, screenX, screenY, pickResult) {
    }

    init {
        this.x = sceneX
        this.y = sceneY
        this.pickResult = pickResult ?: PickResult(target, sceneX, sceneY)
        val p = InputEventUtils.recomputeCoordinates(this.pickResult, null)
        this.x = p.x
        this.y = p.y
        this.z = p.z
    }

    companion object {
        /**
         * Generated Serial Version UID
         */
        private val serialVersionUID = 4413700316447127355L

        /**
         * Common supertype for all dock event types.
         */
        val ANY = EventType<DockEvent>(Event.ANY, "DOCK")

        /**
         * This event occurs when a dock window is being dragged by its title bar and the mouse enters a
         * node's bounds. Unlike a `DragEvent` the dock over event is handed to all stages that may
         * be interested in receiving the dock pane.
         */
        val DOCK_ENTER = EventType(DockEvent.ANY, "DOCK_ENTER")

        /**
         * This event occurs when a dock window is being dragged by its title bar and the mouse is
         * contained in a node's bounds. Unlike a `DragEvent` the dock over event is handed to all
         * stages that may be interested in receiving the dock pane.
         */
        val DOCK_OVER = EventType(DockEvent.ANY, "DOCK_OVER")

        /**
         * This event occurs when a dock window is being dragged by its title bar and the mouse exits a
         * node's bounds. Unlike a `DragEvent` the dock over event is handed to all stages that may
         * be interested in receiving the dock pane.
         */
        val DOCK_EXIT = EventType(DockEvent.ANY, "DOCK_EXIT")

        /**
         * This event occurs when a dock window is being dragged by its title bar and the mouse is
         * released over a node's bounds. Unlike a `DragEvent` the dock over event is handed to all
         * stages that may be interested in receiving the dock pane.
         */
        val DOCK_RELEASED = EventType(DockEvent.ANY, "DOCK_RELEASED")
    }

}
/**
 * Constructs new DockEvent event..
 *
 * @param source the source of the event. Can be null.
 * @param target the target of the event. Can be null.
 * @param eventType The type of the event.
 * @param x The x with respect to the source. Should be in scene coordinates if source == null or
 * source is not a Node.
 * @param y The y with respect to the source. Should be in scene coordinates if source == null or
 * source is not a Node.
 * @param screenX The x coordinate relative to screen.
 * @param screenY The y coordinate relative to screen.
 * @param pickResult pick result. Can be null, in this case a 2D pick result without any further
 * values is constructed based on the scene coordinates
 */
