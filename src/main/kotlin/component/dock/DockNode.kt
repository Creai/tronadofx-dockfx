/**
 * @file DockNode.java
 * @brief Class implementing basic dock node with floating and styling.
 *
 * @section License
 *
 * This file is a part of the DockFX Library. Copyright (C) 2015 Robert B. Colton
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program. If not, see <http:></http:>//www.gnu.org/licenses/>.
 */

package component.dock

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.css.PseudoClass
import javafx.event.EventHandler
import javafx.geometry.Insets
import javafx.geometry.Point2D
import javafx.geometry.Rectangle2D
import javafx.scene.Cursor
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.stage.Screen
import javafx.stage.Stage
import javafx.stage.StageStyle
import javafx.stage.Window

/**
 * Base class for a dock node that provides the layout of the content along with a title bar and a
 * styled border. The dock node can be detached and floated or closed and removed from the layout.
 * Dragging behavior is implemented through the title bar.
 *
 * @since DockFX 0.1
 */
class DockNode
/**
 * Creates a default DockNode with a default title bar and layout.
 *
 * @param contents The contents of the dock node which may be a tree or another scene graph node.
 * @param title The caption title of this dock node which maintains bidirectional state with the
 * title bar and stage.
 * @param graphic The caption graphic of this dock node which maintains bidirectional state with
 * the title bar and stage.
 */
@JvmOverloads constructor(
    /**
     * The contents of the dock node, i.e. a TreeView or ListView.
     */
    private var contents: Node?, title: String? = null, graphic: Node? = null
) : VBox(), EventHandler<MouseEvent> {
    /**
     * The style this dock node should use on its stage when set to floating.
     */
    private var stageStyle = StageStyle.TRANSPARENT
    /**
     * The stage that this dock node is currently using when floating.
     */
    /**
     * The stage associated with this dock node. Can be null if the dock node was never set to
     * floating.
     *
     * @return The stage associated with this node.
     */
    var stage: Stage? = null
        private set
    /**
     * The title bar that implements our dragging and state manipulation.
     */
    private var dockTitleBar: DockTitleBar? = null
    /**
     * The border pane used when floating to provide a styled custom border.
     */
    /**
     * The border pane used to parent this dock node when floating. Can be null if the dock node was
     * never set to floating.
     *
     * @return The stage associated with this node.
     */
    var borderPane: BorderPane? = null
        private set

    /**
     * The dock pane this dock node belongs to when not floating.
     */
    /**
     * The dock pane that was last associated with this dock node. Either the dock pane that it is
     * currently docked to or the one it was detached from. Can be null if the node was never docked.
     *
     * @return The dock pane that was last associated with this dock node.
     */
    var dockPane: DockPane? = null
        private set

    /**
     * Boolean property maintaining whether this node is currently maximized.
     *
     * @defaultValue false
     */
    private val maximizedProperty = object : SimpleBooleanProperty(false) {

        override fun invalidated() {
            this@DockNode.pseudoClassStateChanged(MAXIMIZED_PSEUDO_CLASS, get())
            if (borderPane != null) {
                borderPane!!.pseudoClassStateChanged(MAXIMIZED_PSEUDO_CLASS, get())
            }

            stage!!.isMaximized = get()

            // TODO: This is a work around to fill the screen bounds and not overlap the task bar when
            // the window is undecorated as in Visual Studio. A similar work around needs applied for
            // JFrame in Swing. http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4737788
            // Bug report filed:
            // https://bugs.openjdk.java.net/browse/JDK-8133330
            if (this.get()) {
                val screen = Screen
                    .getScreensForRectangle(stage!!.x, stage!!.y, stage!!.width, stage!!.height)[0]
                val bounds = screen.visualBounds

                stage!!.x = bounds.minX
                stage!!.y = bounds.minY

                stage!!.width = bounds.width
                stage!!.height = bounds.height
            }
        }

        override fun getName(): String {
            return "maximized"
        }
    }

    private val graphicProperty = object : SimpleObjectProperty<Node>() {
        override fun getName(): String {
            return "graphic"
        }
    }

    var graphic: Node
        get() = graphicProperty.get()
        set(graphic) {
            this.graphicProperty.value = graphic
        }

    private val titleProperty = object : SimpleStringProperty("Dock") {
        override fun getName(): String {
            return "title"
        }
    }

    var title: String
        get() = titleProperty.get()
        set(title) {
            this.titleProperty.value = title
        }

    private val customTitleBarProperty = object : SimpleBooleanProperty(true) {
        override fun getName(): String {
            return "customTitleBar"
        }
    }

    val isCustomTitleBar: Boolean
        get() = customTitleBarProperty.get()

    private val floatingProperty = object : SimpleBooleanProperty(false) {
        override fun invalidated() {
            this@DockNode.pseudoClassStateChanged(FLOATING_PSEUDO_CLASS, get())
            if (borderPane != null) {
                borderPane!!.pseudoClassStateChanged(FLOATING_PSEUDO_CLASS, get())
            }
        }

        override fun getName(): String {
            return "floating"
        }
    }

    /**
     * Whether the node is currently floating.
     *
     * @param floating Whether the node is currently floating.
     */
    var isFloating: Boolean
        get() = floatingProperty.get()
        set(floating) = setFloating(floating, null)

    private val floatableProperty = object : SimpleBooleanProperty(true) {
        override fun getName(): String {
            return "floatable"
        }
    }

    var isFloatable: Boolean
        get() = floatableProperty.get()
        set(floatable) {
            if (!floatable && this.isFloating) {
                this.isFloating = false
            }
            this.floatableProperty.set(floatable)
        }

    private val closableProperty = object : SimpleBooleanProperty(true) {
        override fun getName(): String {
            return "closable"
        }
    }

    var isClosable: Boolean
        get() = closableProperty.get()
        set(closable) = this.closableProperty.set(closable)

    private val stageResizableProperty = object : SimpleBooleanProperty(true) {
        override fun getName(): String {
            return "resizable"
        }
    }

    var isStageResizable: Boolean
        get() = stageResizableProperty.get()
        set(resizable) = stageResizableProperty.set(resizable)

    private val dockedProperty = object : SimpleBooleanProperty(false) {
        override fun invalidated() {
            if (get()) {
                if (dockTitleBar != null) {
                    dockTitleBar!!.setVisible(true)
                    dockTitleBar!!.setManaged(true)
                }
            }

            this@DockNode.pseudoClassStateChanged(DOCKED_PSEUDO_CLASS, get())
        }

        override fun getName(): String {
            return "docked"
        }
    }

    val isDocked: Boolean
        get() = dockedProperty.get()

    /**
     * Whether the node is currently maximized.
     *
     * @param maximized Whether the node is currently maximized.
     */
    var isMaximized: Boolean
        get() = maximizedProperty.get()
        set(maximized) = maximizedProperty.set(maximized)

    val isDecorated: Boolean
        get() = stageStyle != StageStyle.TRANSPARENT && stageStyle != StageStyle.UNDECORATED

    /**
     * The last position of the mouse that was within the minimum layout bounds.
     */
    private var sizeLast: Point2D? = null
    /**
     * Whether we are currently resizing in a given direction.
     */
    private var sizeWest = false
    private var sizeEast = false
    private var sizeNorth = false
    private var sizeSouth = false

    /**
     * Gets whether the mouse is currently in this dock node's resize zone.
     *
     * @return Whether the mouse is currently in this dock node's resize zone.
     */
    val isMouseResizeZone: Boolean
        get() = sizeWest || sizeEast || sizeNorth || sizeSouth

    init {
        this.titleProperty.value = title
        this.graphicProperty.value = graphic

        dockTitleBar = DockTitleBar(this)

        children.addAll(dockTitleBar, contents)
        VBox.setVgrow(contents, Priority.ALWAYS)

        this.styleClass.add("dock-node")
    }

    /**
     * The stage style that will be used when the dock node is floating. This must be set prior to
     * setting the dock node to floating.
     *
     * @param stageStyle The stage style that will be used when the node is floating.
     */
    fun setStageStyle(stageStyle: StageStyle) {
        this.stageStyle = stageStyle
    }

    /**
     * Changes the contents of the dock node.
     *
     * @param contents The new contents of this dock node.
     */
    fun setContents(contents: Node) {
        this.children[this.children.indexOf(this.contents)] = contents
        this.contents = contents
    }

    /**
     * Changes the title bar in the layout of this dock node. This can be used to remove the dock
     * title bar from the dock node by passing null.
     *
     * @param dockTitleBar null The new title bar of this dock node, can be set null indicating no
     * title bar is used.
     */
    fun setDockTitleBar(dockTitleBar: DockTitleBar?) {
        if (dockTitleBar != null) {
            if (this.dockTitleBar != null) {
                this.children[this.children.indexOf(this.dockTitleBar)] = dockTitleBar
            } else {
                this.children.add(0, dockTitleBar)
            }
        } else {
            this.children.remove(this.dockTitleBar)
        }

        this.dockTitleBar = dockTitleBar
    }

    /**
     * Whether the node is currently floating.
     *
     * @param floating Whether the node is currently floating.
     * @param translation null The offset of the node after being set floating. Used for aligning it
     * with its layout bounds inside the dock pane when it becomes detached. Can be null
     * indicating no translation.
     */
    fun setFloating(floating: Boolean, translation: Point2D?) {
        if (floating && !this.isFloating) {
            // position the new stage relative to the old scene offset
            val floatScene = this.localToScene(0.0, 0.0)
            val floatScreen = this.localToScreen(0.0, 0.0)

            // setup window stage
            dockTitleBar!!.setVisible(this.isCustomTitleBar)
            dockTitleBar!!.setManaged(this.isCustomTitleBar)

            // apply the floating property so we can get its padding size
            // while it is floating to offset it by the drop shadow
            // this way it pops out above exactly where it was when docked
            this.floatingProperty.set(floating)
            this.applyCss()

            if (this.isDocked) {
                this.undock()
            }

            stage = Stage()
            stage!!.titleProperty().bind(titleProperty)
            if (dockPane != null && dockPane!!.getScene() != null
                && dockPane!!.getScene().getWindow() != null
            ) {
                stage!!.initOwner(dockPane!!.getScene().getWindow())
            }

            stage!!.initStyle(stageStyle)

            // offset the new stage to cover exactly the area the dock was local to the scene
            // this is useful for when the user presses the + sign and we have no information
            // on where the mouse was clicked
            var stagePosition: Point2D
            if (this.isDecorated) {
                val owner = stage!!.owner
                stagePosition = floatScene.add(Point2D(owner.x, owner.y))
            } else {
                stagePosition = floatScreen
            }
            if (translation != null) {
                stagePosition = stagePosition.add(translation)
            }

            // the border pane allows the dock node to
            // have a drop shadow effect on the border
            // but also maintain the layout of contents
            // such as a tab that has no content
            borderPane = BorderPane()
            borderPane!!.styleClass.add("dock-node-border")
            borderPane!!.center = this

            val scene = Scene(borderPane!!)

            // apply the border pane css so that we can get the insets and
            // position the stage properly
            borderPane!!.applyCss()
            val insetsDelta = borderPane!!.insets

            val insetsWidth = insetsDelta.left + insetsDelta.right
            val insetsHeight = insetsDelta.top + insetsDelta.bottom

            stage!!.x = stagePosition.x - insetsDelta.left
            stage!!.y = stagePosition.y - insetsDelta.top

            stage!!.minWidth = borderPane!!.minWidth(this.height) + insetsWidth
            stage!!.minHeight = borderPane!!.minHeight(this.width) + insetsHeight

            borderPane!!.setPrefSize(this.width + insetsWidth, this.height + insetsHeight)

            stage!!.scene = scene

            if (stageStyle == StageStyle.TRANSPARENT) {
                scene.fill = null
            }

            stage!!.isResizable = this.isStageResizable
            if (this.isStageResizable) {
                stage!!.addEventFilter(MouseEvent.MOUSE_PRESSED, this)
                stage!!.addEventFilter(MouseEvent.MOUSE_MOVED, this)
                stage!!.addEventFilter(MouseEvent.MOUSE_DRAGGED, this)
            }

            // we want to set the client area size
            // without this it subtracts the native border sizes from the scene
            // size
            stage!!.sizeToScene()

            stage!!.show()
        } else if (!floating && this.isFloating) {
            this.floatingProperty.set(floating)

            stage!!.removeEventFilter(MouseEvent.MOUSE_PRESSED, this)
            stage!!.removeEventFilter(MouseEvent.MOUSE_MOVED, this)
            stage!!.removeEventFilter(MouseEvent.MOUSE_DRAGGED, this)

            stage!!.close()
        }
    }

    /**
     * The dock title bar associated with this dock node.
     *
     * @return The dock title bar associated with this node.
     */
    fun getDockTitleBar(): DockTitleBar? {
        return this.dockTitleBar
    }

    /**
     * The contents managed by this dock node.
     *
     * @return The contents managed by this dock node.
     */
    fun getContents(): Node? {
        return contents
    }

    /**
     * Object property maintaining bidirectional state of the caption graphic for this node with the
     * dock title bar or stage.
     *
     * @defaultValue null
     */
    fun graphicProperty(): ObjectProperty<Node> {
        return graphicProperty
    }

    /**
     * Boolean property maintaining bidirectional state of the caption title for this node with the
     * dock title bar or stage.
     *
     * @defaultValue "Dock"
     */
    fun titleProperty(): StringProperty {
        return titleProperty
    }

    /**
     * Boolean property maintaining whether this node is currently using a custom title bar. This can
     * be used to force the default title bar to show when the dock node is set to floating instead of
     * using native window borders.
     *
     * @defaultValue true
     */
    fun customTitleBarProperty(): BooleanProperty {
        return customTitleBarProperty
    }

    fun setUseCustomTitleBar(useCustomTitleBar: Boolean) {
        if (this.isFloating) {
            dockTitleBar!!.setVisible(useCustomTitleBar)
            dockTitleBar!!.setManaged(useCustomTitleBar)
        }
        this.customTitleBarProperty.set(useCustomTitleBar)
    }

    /**
     * Boolean property maintaining whether this node is currently floating.
     *
     * @defaultValue false
     */
    fun floatingProperty(): BooleanProperty {
        return floatingProperty
    }

    /**
     * Boolean property maintaining whether this node is currently floatable.
     *
     * @defaultValue true
     */
    fun floatableProperty(): BooleanProperty {
        return floatableProperty
    }

    /**
     * Boolean property maintaining whether this node is currently closable.
     *
     * @defaultValue true
     */
    fun closableProperty(): BooleanProperty {
        return closableProperty
    }

    /**
     * Boolean property maintaining whether this node is currently resizable.
     *
     * @defaultValue true
     */
    fun resizableProperty(): BooleanProperty {
        return stageResizableProperty
    }

    /**
     * Boolean property maintaining whether this node is currently docked. This is used by the dock
     * pane to inform the dock node whether it is currently docked.
     *
     * @defaultValue false
     */
    fun dockedProperty(): BooleanProperty {
        return dockedProperty
    }

    fun maximizedProperty(): BooleanProperty {
        return maximizedProperty
    }

    /**
     * Dock this node into a dock pane.
     *
     * @param dockPane The dock pane to dock this node into.
     * @param dockPos The docking position relative to the sibling of the dock pane.
     * @param sibling The sibling node to dock this node relative to.
     */
    fun dock(dockPane: DockPane, dockPos: DockPos, sibling: Node) {
        dockImpl(dockPane)
        dockPane.dock(this, dockPos, sibling)
    }

    /**
     * Dock this node into a dock pane.
     *
     * @param dockPane The dock pane to dock this node into.
     * @param dockPos The docking position relative to the sibling of the dock pane.
     */
    fun dock(dockPane: DockPane, dockPos: DockPos) {
        dockImpl(dockPane)
        dockPane.dock(this, dockPos)
    }

    private fun dockImpl(dockPane: DockPane) {
        if (isFloating) {
            isFloating = false
        }
        this.dockPane = dockPane
        this.dockedProperty.set(true)
    }

    /**
     * Detach this node from its previous dock pane if it was previously docked.
     */
    fun undock() {
        if (dockPane != null) {
            dockPane!!.undock(this)
        }
        this.dockedProperty.set(false)
    }

    /**
     * Close this dock node by setting it to not floating and making sure it is detached from any dock
     * pane.
     */
    fun close() {
        if (isFloating) {
            isFloating = false
        } else if (isDocked) {
            undock()
        }
    }

    override fun handle(event: MouseEvent) {
        var cursor = Cursor.DEFAULT

        // TODO: use escape to cancel resize/drag operation like visual studio
        if (!this.isFloating || !this.isStageResizable) {
            return
        }

        if (event.eventType == MouseEvent.MOUSE_PRESSED) {
            sizeLast = Point2D(event.screenX, event.screenY)
        } else if (event.eventType == MouseEvent.MOUSE_MOVED) {
            val insets = borderPane!!.padding

            sizeWest = event.x < insets.left
            sizeEast = event.x > borderPane!!.width - insets.right
            sizeNorth = event.y < insets.top
            sizeSouth = event.y > borderPane!!.height - insets.bottom

            if (sizeWest) {
                if (sizeNorth) {
                    cursor = Cursor.NW_RESIZE
                } else if (sizeSouth) {
                    cursor = Cursor.SW_RESIZE
                } else {
                    cursor = Cursor.W_RESIZE
                }
            } else if (sizeEast) {
                if (sizeNorth) {
                    cursor = Cursor.NE_RESIZE
                } else if (sizeSouth) {
                    cursor = Cursor.SE_RESIZE
                } else {
                    cursor = Cursor.E_RESIZE
                }
            } else if (sizeNorth) {
                cursor = Cursor.N_RESIZE
            } else if (sizeSouth) {
                cursor = Cursor.S_RESIZE
            }

            this.scene.cursor = cursor
        } else if (event.eventType == MouseEvent.MOUSE_DRAGGED && this.isMouseResizeZone) {
            val sizeCurrent = Point2D(event.screenX, event.screenY)
            val sizeDelta = sizeCurrent.subtract(sizeLast!!)

            var newX = stage!!.x
            var newY = stage!!.y
            var newWidth = stage!!.width
            var newHeight = stage!!.height

            if (sizeNorth) {
                newHeight -= sizeDelta.y
                newY += sizeDelta.y
            } else if (sizeSouth) {
                newHeight += sizeDelta.y
            }

            if (sizeWest) {
                newWidth -= sizeDelta.x
                newX += sizeDelta.x
            } else if (sizeEast) {
                newWidth += sizeDelta.x
            }

            // TODO: find a way to do this synchronously and eliminate the flickering of moving the stage
            // around, also file a bug report for this feature if a work around can not be found this
            // primarily occurs when dragging north/west but it also appears in native windows and Visual
            // Studio, so not that big of a concern.
            // Bug report filed:
            // https://bugs.openjdk.java.net/browse/JDK-8133332
            var currentX = sizeLast!!.x
            var currentY = sizeLast!!.y
            if (newWidth >= stage!!.minWidth) {
                stage!!.x = newX
                stage!!.width = newWidth
                currentX = sizeCurrent.x
            }

            if (newHeight >= stage!!.minHeight) {
                stage!!.y = newY
                stage!!.height = newHeight
                currentY = sizeCurrent.y
            }
            sizeLast = Point2D(currentX, currentY)
            // we do not want the title bar getting these events
            // while we are actively resizing
            if (sizeNorth || sizeSouth || sizeWest || sizeEast) {
                event.consume()
            }
        }
    }

    companion object {

        /**
         * CSS pseudo class selector representing whether this node is currently floating.
         */
        private val FLOATING_PSEUDO_CLASS = PseudoClass.getPseudoClass("floating")
        /**
         * CSS pseudo class selector representing whether this node is currently docked.
         */
        private val DOCKED_PSEUDO_CLASS = PseudoClass.getPseudoClass("docked")
        /**
         * CSS pseudo class selector representing whether this node is currently maximized.
         */
        private val MAXIMIZED_PSEUDO_CLASS = PseudoClass.getPseudoClass("maximized")
    }
}
/**
 * Creates a default DockNode with a default title bar and layout.
 *
 * @param contents The contents of the dock node which may be a tree or another scene graph node.
 * @param title The caption title of this dock node which maintains bidirectional state with the
 * title bar and stage.
 */
/**
 * Creates a default DockNode with a default title bar and layout.
 *
 * @param contents The contents of the dock node which may be a tree or another scene graph node.
 */
