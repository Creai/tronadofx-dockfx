/**
 * @file DockPane.java
 * @brief Class implementing a generic dock pane for the layout of dock nodes.
 *
 * @section License
 *
 * This file is a part of the DockFX Library. Copyright (C) 2015 Robert B. Colton
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * program. If not, see <http:></http:>//www.gnu.org/licenses/>.
 */

package component.dock

import java.util.ArrayList
import java.util.Stack

import com.sun.javafx.css.StyleManager

import javafx.animation.KeyFrame
import javafx.animation.KeyValue
import javafx.animation.Timeline
import javafx.application.Application
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.collections.ObservableMap
import javafx.css.PseudoClass
import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.geometry.Point2D
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.control.Button
import javafx.scene.control.SplitPane
import javafx.scene.layout.GridPane
import javafx.scene.layout.StackPane
import javafx.scene.shape.Rectangle
import javafx.stage.Popup
import javafx.util.Duration
import tornadofx.FX
import tornadofx.Stylesheet.Companion.button
import tornadofx.add
import tornadofx.importStylesheet
import java.lang.UnsupportedOperationException

/**
 * Base class for a dock pane that provides the layout of the dock nodes. Stacking the dock nodes to
 * the center in a TabPane will be added in a future release. For now the DockPane uses the relative
 * sizes of the dock nodes and lays them out in a tree of SplitPanes.
 *
 * @since DockFX 0.1
 */
class DockPane : StackPane(), EventHandler<DockEvent> {

    /**
     * The current root node of this dock pane's layout.
     */
    private var root: Node? = null

    /**
     * Whether a DOCK_ENTER event has been received by this dock pane since the last DOCK_EXIT event
     * was received.
     */
    private var receivedEnter = false

    /**
     * The current node in this dock pane that we may be dragging over.
     */
    private var dockNodeDrag: Node? = null
    /**
     * The docking area of the current dock indicator button if any is selected. This is either the
     * root or equal to dock node drag.
     */
    private var dockAreaDrag: Node? = null
    /**
     * The docking position of the current dock indicator button if any is selected.
     */
    private var dockPosDrag: DockPos? = null

    /**
     * The docking area shape with a dotted animated border on the indicator overlay popup.
     */
    private val dockAreaIndicator: Rectangle
    /**
     * The timeline used to animate the borer of the docking area indicator shape. Because JavaFX has
     * no CSS styling for timelines/animations yet we will make this private and offer an accessor for
     * the user to programmatically modify the animation or disable it.
     */
    /**
     * The Timeline used to animate the docking area indicator in the dock indicator overlay for this
     * dock pane.
     *
     * @return The Timeline used to animate the docking area indicator in the dock indicator overlay
     * for this dock pane.
     */
    val dockAreaStrokeTimeline: Timeline
    /**
     * The popup used to display the root dock indicator buttons and the docking area indicator.
     */
    private val dockIndicatorOverlay: Popup

    /**
     * The grid pane used to lay out the local dock indicator buttons. This is the grid used to lay
     * out the buttons in the circular indicator.
     */
    private val dockPosIndicator: GridPane
    /**
     * The popup used to display the local dock indicator buttons. This allows these indicator buttons
     * to be displayed outside the window of this dock pane.
     */
    private val dockIndicatorPopup: Popup

    /**
     * A collection used to manage the indicator buttons and automate hit detection during DOCK_OVER
     * events.
     */
    private val dockPosButtons: ObservableList<DockPosButton>

    /**
     * A cache of all dock node event handlers that we have created for tracking the current docking
     * area.
     */
    private val dockNodeEventFilters = FXCollections.observableHashMap<Node, DockNodeEventHandler>()

    /**
     * Base class for a dock indicator button that allows it to be displayed during a dock event and
     * continue to receive input.
     *
     * @since DockFX 0.1
     */
    inner class DockPosButton
    /**
     * Creates a new dock indicator button.
     */
        (dockRoot: Boolean, dockPos: DockPos) : Button() {
        /**
         * Whether this dock indicator button is used for docking a node relative to the root of the
         * dock pane.
         */
        /**
         * Whether this dock indicator button is used for docking a node relative to the root of the
         * dock pane.
         *
         * @return Whether this indicator button is used for docking a node relative to the root of the
         * dock pane.
         */
        /**
         * Whether this dock indicator button is used for docking a node relative to the root of the
         * dock pane.
         *
         * @param dockRoot Whether this indicator button is used for docking a node relative to the root
         * of the dock pane.
         */
        var isDockRoot = true
        /**
         * The docking position indicated by this button.
         */
        /**
         * The docking position indicated by this button.
         *
         * @return The docking position indicated by this button.
         */
        /**
         * The docking position indicated by this button.
         *
         * @param dockPos The docking position indicated by this button.
         */
        var dockPos = DockPos.CENTER

        init {
            this.isDockRoot = dockRoot
            this.dockPos = dockPos
        }
    }

    /**
     * Creates a new DockPane adding event handlers for dock events and creating the indicator
     * overlays.
     */
    init {
        DockPane.dockPanes.add(this)

        this.addEventHandler<DockEvent>(DockEvent.ANY, this)
        this.addEventFilter<DockEvent>(DockEvent.ANY) { event ->
            if (event.getEventType() === DockEvent.DOCK_ENTER) {
                this@DockPane.receivedEnter = true
            } else if (event.getEventType() === DockEvent.DOCK_OVER) {
                this@DockPane.dockNodeDrag = null
            }
        }

        dockIndicatorPopup = Popup()
        dockIndicatorPopup.isAutoFix = false

        dockIndicatorOverlay = Popup()
        dockIndicatorOverlay.isAutoFix = false

        val dockRootPane = StackPane()
        dockRootPane.prefWidthProperty().bind(this.widthProperty())
        dockRootPane.prefHeightProperty().bind(this.heightProperty())

        dockAreaIndicator = Rectangle()
        dockAreaIndicator.isManaged = false
        dockAreaIndicator.isMouseTransparent = true

        dockAreaStrokeTimeline = Timeline()
        dockAreaStrokeTimeline.cycleCount = Timeline.INDEFINITE
        // 12 is the cumulative offset of the stroke dash array in the default.css style sheet
        // RFE filed for CSS styled timelines/animations:
        // https://bugs.openjdk.java.net/browse/JDK-8133837
        val kv = KeyValue(dockAreaIndicator.strokeDashOffsetProperty(), 12)
        val kf = KeyFrame(Duration.millis(500.0), kv)
        dockAreaStrokeTimeline.keyFrames.add(kf)
        dockAreaStrokeTimeline.play()

        val dockCenter = DockPosButton(false, DockPos.CENTER)
        dockCenter.styleClass.add("dock-center")

        val dockTop = DockPosButton(false, DockPos.TOP)
        dockTop.styleClass.add("dock-top")
        val dockRight = DockPosButton(false, DockPos.RIGHT)
        dockRight.styleClass.add("dock-right")
        val dockBottom = DockPosButton(false, DockPos.BOTTOM)
        dockBottom.styleClass.add("dock-bottom")
        val dockLeft = DockPosButton(false, DockPos.LEFT)
        dockLeft.styleClass.add("dock-left")

        val dockTopRoot = DockPosButton(true, DockPos.TOP)
        StackPane.setAlignment(dockTopRoot, Pos.TOP_CENTER)
        dockTopRoot.styleClass.add("dock-top-root")

        val dockRightRoot = DockPosButton(true, DockPos.RIGHT)
        StackPane.setAlignment(dockRightRoot, Pos.CENTER_RIGHT)
        dockRightRoot.styleClass.add("dock-right-root")

        val dockBottomRoot = DockPosButton(true, DockPos.BOTTOM)
        StackPane.setAlignment(dockBottomRoot, Pos.BOTTOM_CENTER)
        dockBottomRoot.styleClass.add("dock-bottom-root")

        val dockLeftRoot = DockPosButton(true, DockPos.LEFT)
        StackPane.setAlignment(dockLeftRoot, Pos.CENTER_LEFT)
        dockLeftRoot.styleClass.add("dock-left-root")

        // TODO: dockCenter goes first when tabs are added in a future version
        dockPosButtons = FXCollections.observableArrayList(
            dockTop, dockRight, dockBottom, dockLeft,
            dockTopRoot, dockRightRoot, dockBottomRoot, dockLeftRoot
        )

        dockPosIndicator = GridPane()
        dockPosIndicator.add(dockTop, 1, 0)
        dockPosIndicator.add(dockRight, 2, 1)
        dockPosIndicator.add(dockBottom, 1, 2)
        dockPosIndicator.add(dockLeft, 0, 1)
        // dockPosIndicator.add(dockCenter, 1, 1);

        dockRootPane.children.addAll(
            dockAreaIndicator, dockTopRoot, dockRightRoot, dockBottomRoot,
            dockLeftRoot
        )

        dockIndicatorOverlay.content.add(dockRootPane)
        dockIndicatorPopup.content.addAll(dockPosIndicator)

        this.styleClass.add("dock-pane")
        dockRootPane.styleClass.add("dock-root-pane")
        dockPosIndicator.styleClass.add("dock-pos-indicator")
        dockAreaIndicator.styleClass.add("dock-area-indicator")
    }

    /**
     * A wrapper to the type parameterized generic EventHandler that allows us to remove it from its
     * listener when the dock node becomes detached. It is specifically used to monitor which dock
     * node in this dock pane's layout we are currently dragging over.
     *
     * @since DockFX 0.1
     */
    private inner class DockNodeEventHandler
    /**
     * Creates a default dock node event handler that will help this dock pane track the current
     * docking area.
     *
     * @param node The node that is to listen for docking events and report to the encapsulating
     * docking pane.
     */
        (
        /**
         * The node associated with this event handler that reports to the encapsulating dock pane.
         */
        private val node: Node
    ) : EventHandler<DockEvent> {

        override fun handle(event: DockEvent) {
                this@DockPane.dockNodeDrag = node
            }
        }

    /**
     * Dock the node into this dock pane at the given docking position relative to the sibling in the
     * layout. This is used to relatively position the dock nodes to other nodes given their preferred
     * size.
     *
     * @param node The node that is to be docked into this dock pane.
     * @param dockPos The docking position of the node relative to the sibling.
     * @param sibling The sibling of this node in the layout.
     */
    @JvmOverloads
    fun dock(node: Node, dockPos: DockPos, sibling: Node? = root) {
        val dockNodeEventHandler = DockNodeEventHandler(node)
        dockNodeEventFilters[node] = dockNodeEventHandler
        node.addEventFilter<DockEvent>(DockEvent.DOCK_OVER, dockNodeEventHandler)

        var split = root as SplitPane?
        if (split == null) {
            split = SplitPane()
            split.items.add(node)
            root = split
            this.children.add(root)
            return
        }

        // find the parent of the sibling
        if (sibling != null && sibling !== root) {
            val stack = Stack<Parent>()
            stack.push(root as Parent?)
            while (!stack.isEmpty()) {
                val parent = stack.pop()

                var children = parent.childrenUnmodifiable

                if (parent is SplitPane) {
                    children = parent.items
                }

                for (i in children.indices) {
                    if (children[i] === sibling) {
                        split = parent as SplitPane
                    } else if (children[i] is Parent) {
                        stack.push(children[i] as Parent)
                    }
                }
            }
        }

        val requestedOrientation = if (dockPos === DockPos.LEFT || dockPos === DockPos.RIGHT)
            Orientation.HORIZONTAL
        else
            Orientation.VERTICAL

        // if the orientation is different then reparent the split pane
        if (split!!.orientation != requestedOrientation) {
            if (split.items.size > 1) {
                val splitPane = SplitPane()
                if (split === root && sibling === root) {
                    this.children[this.children.indexOf(root)] = splitPane
                    splitPane.items.add(split)
                    root = splitPane
                } else {
                    split.items[split.items.indexOf(sibling)] = splitPane
                    splitPane.items.add(sibling)
                }

                split = splitPane
            }
            split.orientation = requestedOrientation
        }

        // finally dock the node to the correct split pane
        val splitItems = split.items

        var magnitude = 0.0

        if (splitItems.size > 0) {
            if (split.orientation == Orientation.HORIZONTAL) {
                for (splitItem in splitItems) {
                    magnitude += splitItem.prefWidth(0.0)
                }
            } else {
                for (splitItem in splitItems) {
                    magnitude += splitItem.prefHeight(0.0)
                }
            }
        }

        if (dockPos === DockPos.LEFT || dockPos === DockPos.TOP) {
            var relativeIndex = 0
            if (sibling != null && sibling !== root) {
                relativeIndex = splitItems.indexOf(sibling)
            }

            splitItems.add(relativeIndex, node)

            if (splitItems.size > 1) {
                if (split.orientation == Orientation.HORIZONTAL) {
                    split.setDividerPosition(
                        relativeIndex,
                        node.prefWidth(0.0) / (magnitude + node.prefWidth(0.0))
                    )
                } else {
                    split.setDividerPosition(
                        relativeIndex,
                        node.prefHeight(0.0) / (magnitude + node.prefHeight(0.0))
                    )
                }
            }
        } else if (dockPos === DockPos.RIGHT || dockPos === DockPos.BOTTOM) {
            var relativeIndex = splitItems.size
            if (sibling != null && sibling !== root) {
                relativeIndex = splitItems.indexOf(sibling) + 1
            }

            splitItems.add(relativeIndex, node)
            if (splitItems.size > 1) {
                if (split.orientation == Orientation.HORIZONTAL) {
                    split.setDividerPosition(
                        relativeIndex - 1,
                        1 - node.prefWidth(0.0) / (magnitude + node.prefWidth(0.0))
                    )
                } else {
                    split.setDividerPosition(
                        relativeIndex - 1,
                        1 - node.prefHeight(0.0) / (magnitude + node.prefHeight(0.0))
                    )
                }
            }
        }

    }

    /**
     * Detach the node from this dock pane removing it from the layout.
     *
     * @param node The node that is to be removed from this dock pane.
     */
    fun undock(node: DockNode) {
        val dockNodeEventHandler = dockNodeEventFilters[node]
        node.removeEventFilter(DockEvent.DOCK_OVER, dockNodeEventHandler)
        dockNodeEventFilters.remove(node)

        // depth first search to find the parent of the node
        val findStack = Stack<Parent>()
        findStack.push(root as Parent?)
        while (!findStack.isEmpty()) {
            var parent = findStack.pop()

            var children = parent.childrenUnmodifiable

            if (parent is SplitPane) {
                children = parent.items
            }

            var i = 0
            while (i < children.size) {
                if (children[i] === node) {
                    children.removeAt(i)

                    // start from the root again and remove any SplitPane's with no children in them
                    val clearStack = Stack<Parent>()
                    clearStack.push(root as Parent?)
                    while (!clearStack.isEmpty()) {
                        parent = clearStack.pop()

                        children = parent.childrenUnmodifiable

                        if (parent is SplitPane) {
                            children = parent.items
                        }

                        i = 0
                        while (i < children.size) {
                            if (children[i] is SplitPane) {
                                val split = children[i] as SplitPane
                                if (split.items.size < 1) {
                                    children.removeAt(i)
                                    i++
                                    continue
                                } else {
                                    clearStack.push(split)
                                }
                            }
                            i++

                        }
                    }

                    return
                } else if (children[i] is Parent) {
                    findStack.push(children[i] as Parent)
                }
                i++
            }
        }
    }

    override fun handle(event: DockEvent) {
        if (event.getEventType() === DockEvent.DOCK_ENTER) {
            if (!dockIndicatorOverlay.isShowing) {
                val topLeft = this@DockPane.localToScreen(0.0, 0.0)
                dockIndicatorOverlay.show(this@DockPane, topLeft.x, topLeft.y)
            }
        } else if (event.getEventType() === DockEvent.DOCK_OVER) {
            this.receivedEnter = false

            dockPosDrag = null
            dockAreaDrag = dockNodeDrag

            for (dockIndicatorButton in dockPosButtons) {
                if (dockIndicatorButton
                        .contains(dockIndicatorButton.screenToLocal(event.screenX, event.screenY))
                ) {
                    dockPosDrag = dockIndicatorButton.dockPos
                    if (dockIndicatorButton.isDockRoot) {
                        dockAreaDrag = root
                    }
                    dockIndicatorButton.pseudoClassStateChanged(PseudoClass.getPseudoClass("focused"), true)
                    break
                } else {
                    dockIndicatorButton.pseudoClassStateChanged(PseudoClass.getPseudoClass("focused"), false)
                }
            }

            if (dockPosDrag != null) {
                val originToScene = dockAreaDrag!!.localToScene(0.0, 0.0).subtract(this.localToScene(0.0, 0.0))

                dockAreaIndicator.isVisible = true
                dockAreaIndicator.relocate(originToScene.x, originToScene.y)
                if (dockPosDrag === DockPos.RIGHT) {
                    dockAreaIndicator.translateX = dockAreaDrag!!.layoutBounds.width / 2
                } else {
                    dockAreaIndicator.translateX = 0.0
                }

                if (dockPosDrag === DockPos.BOTTOM) {
                    dockAreaIndicator.translateY = dockAreaDrag!!.layoutBounds.height / 2
                } else {
                    dockAreaIndicator.translateY = 0.0
                }

                if (dockPosDrag === DockPos.LEFT || dockPosDrag === DockPos.RIGHT) {
                    dockAreaIndicator.width = dockAreaDrag!!.layoutBounds.width / 2
                } else {
                    dockAreaIndicator.width = dockAreaDrag!!.layoutBounds.width
                }
                if (dockPosDrag === DockPos.TOP || dockPosDrag === DockPos.BOTTOM) {
                    dockAreaIndicator.height = dockAreaDrag!!.layoutBounds.height / 2
                } else {
                    dockAreaIndicator.height = dockAreaDrag!!.layoutBounds.height
                }
            } else {
                dockAreaIndicator.isVisible = false
            }

            if (dockNodeDrag != null) {
                val originToScreen = dockNodeDrag!!.localToScreen(0.0, 0.0)

                val posX = originToScreen.x + dockNodeDrag!!.layoutBounds.width / 2 - dockPosIndicator.width / 2
                val posY = originToScreen.y + dockNodeDrag!!.layoutBounds.height / 2 - dockPosIndicator.height / 2

                if (!dockIndicatorPopup.isShowing) {
                    dockIndicatorPopup.show(this@DockPane, posX, posY)
                } else {
                    dockIndicatorPopup.x = posX
                    dockIndicatorPopup.y = posY
                }

                // set visible after moving the popup
                dockPosIndicator.isVisible = true
            } else {
                dockPosIndicator.isVisible = false
            }
        }

        if (event.getEventType() === DockEvent.DOCK_RELEASED && event.contents != null) {
            if (dockPosDrag != null && dockIndicatorOverlay.isShowing) {
                val dockNode = event.contents as DockNode
                dockNode.dock(this, dockPosDrag!!, dockAreaDrag!!)
            }
        }

        if (event.getEventType() === DockEvent.DOCK_EXIT && !this.receivedEnter || event.getEventType() === DockEvent.DOCK_RELEASED) {
            if (dockIndicatorPopup.isShowing) {
                dockIndicatorOverlay.hide()
                dockIndicatorPopup.hide()
            }
        }
    }

    companion object {
        /**
         * Package-private internal list of all DockPanes for event mouse picking.
         */
        internal var dockPanes: MutableList<DockPane> = ArrayList()
    }
}
/**
 * Dock the node into this dock pane at the given docking position relative to the root in the
 * layout. This is used to relatively position the dock nodes to other nodes given their preferred
 * size.
 *
 * @param node The node that is to be docked into this dock pane.
 * @param dockPos The docking position of the node relative to the sibling.
 */
