package app

import tornadofx.App
import tornadofx.launch
import view.RootView

class MainApplication : App(RootView::class, Styles::class)
